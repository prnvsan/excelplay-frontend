import React, { useEffect, useState } from 'react';
import '../../../App.scss';
import './KryptosPlay.scss';
import KryptosInfoBar from '../KryptosInfoBar/KryptosInfoBar';
import KryptosQuestion from '../KryptosQuestion/KryptosQuestion';
import KryptosHintModal from '../KryptosHintModal/KryptosHintModal';
import KryptosRanklist from '../KryptosRanklist/KryptosRanklist';
import {
  NO_LEVELS_LEFT,
  MESSAGE_WHEN_ALL_LEVELS_COMPLETE,
  MESSAGE_WHEN_CORRECT_ANSWER,
  MESSAGE_WHEN_WRONG_ANSWER,
  //NO_HINTS,
} from '../../../components/common/Constants'
import {
  fetchQuestion,
  fetchRank,
  submitKryptosAnswer,
  getStaticAsset,
} from '../KryptosApi/ApiCalls';
import { useSelector, useDispatch } from 'react-redux';
import { rootType } from '../../../store/Reducers/rootReducer';

const KryptosPlay = () => {
  //=====FOR USING DUMMY DATA==========//
  const [level, setLevel] = useState(1);
  const [rank, setRank] = useState(0);
  const [imgUrl, setImgUrl] = useState('');
  const [sourceHint, setSourceHint] = useState('');
  // const [hintText, setHintText] = useState([NO_HINTS]);
  const [hintText, setHintText] = useState(['']);

  // Uncomment this line when using API.
  // const {level, rank, imgUrl, sourceHint, hintText} = useSelector((state: rootType) => state.Kryptos);
  const dispatch = useDispatch();

  useEffect(() => {
    // fetchQuestion().then(data => {
    //   if (!data.completed) {
    //     if (data.filetype !== 'NI') {
    //       //setImgUrl(getStaticAsset(data.level_file));
    //       dispatch({
    //         type: 'SET_IMG_URL',
    //         payload: getStaticAsset(data.level_file)
    //       });
    //     }
    //     //setLevel(data.level);
    //     dispatch({
    //       type: 'SET_LEVEL',
    //       payload: data.level
    //     });
    //     //setSourceHint(data.source_hint);
    //     dispatch({
    //       type: 'SET_SOURCE_HINT',
    //       payload: data.source_hint
    //     });
    //     if (data.hints.length) {
    //       const hints = data.hints.map((e: { hint: any; }) => {
    //         return e.hint;
    //       });
    //       //setHintText(hints);
    //       dispatch({
    //         type: 'SET_HINT_TEXT',
    //         payload: hints
    //       });
    //     }
    //   } else {
    //     window.alert(MESSAGE_WHEN_ALL_LEVELS_COMPLETE);
    //     //setLevel(parseInt(NO_LEVELS_LEFT));
    //     dispatch({
    //       type: 'SET_LEVEL',
    //       payload: parseInt(NO_LEVELS_LEFT)
    //     });
    //   }
    // });

    // fetchRank().then(data => {
    //   if (data.kryptos) {
    //     dispatch({
    //       type: 'SET_RANK',
    //       payload: data.kryptos.rank
    //     });
    //   } //setRank(data.kryptos.rank);
    //   else{
    //     dispatch({
    //       type: 'SET_RANK',
    //       payload: 1
    //     });
    //   } //setRank(1);
    // });

    //====SOME DUMMY DATA=====//
    setImgUrl("https://www.globalpharmatek.com/wp-content/uploads/2016/10/orionthemes-placeholder-image.jpg");
    setSourceHint("This is a source_hint");
    setHintText(["hint1", "hint2"])

  }, [dispatch]);

  const onSubmit = (ans: string) => {
    // submitKryptosAnswer(ans).then(data => {
    //   if (data.answer === 'Correct') {
    //     window.alert(MESSAGE_WHEN_CORRECT_ANSWER);
    //     setTimeout(() => {
    //       window.location.reload();
    //     }, 1000);
    //   } else {
    //     window.alert(MESSAGE_WHEN_WRONG_ANSWER);
    //   }
    // });
    //====TEMPORARY, CHECKING IF ANSWER IS CORRECT====//
    if (ans === "correct") {
      window.alert(MESSAGE_WHEN_CORRECT_ANSWER);
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } else {
      window.alert(MESSAGE_WHEN_WRONG_ANSWER);
    }
  };
 
  
  return (
    <div>
      <div className="row">
       <div className="col">
        <KryptosInfoBar level={level} rank={rank} />
       </div>
       </div>
       <div className= "row">
      <div className="col-lg-4 offset-lg-4">
        <KryptosQuestion
          imgUrl={imgUrl}
          sourceHint={sourceHint}
          onSubmit={(ans: string) => onSubmit(ans)}
        />
      </div>
        <div className="col-lg-4">
       <span className = "rank">
      <KryptosRanklist/>
      </span>
      </div>
      </div>
      <KryptosHintModal hintText={hintText} />
    </div>
  );
};

export default KryptosPlay;

import React, { useEffect, useState } from 'react';
import './KryptosRanklist.scss';
import { fetchKryptosLeaderboard } from '../KryptosApi/ApiCalls';
import { useDispatch, useSelector } from 'react-redux';
import { rootType } from '../../../store/Reducers/rootReducer';

interface rankList {
  pic?: string;
  name?: string;
  rank?: string;
  level?: string;
}

const KryptosRanklist = () => {
  // =====FOR TEMPORARY DUMMY DATA====== //
  const [ranklist, setRanklist] = useState<rankList[]>([]);

  // Uncomment below line when removing dummy data and using backend.
  // const {ranklist} = useSelector((state: rootType) => state.Kryptos);
  const dispatch = useDispatch();

  useEffect(() => {
    // fetchKryptosLeaderboard().then(data => {
    //   //setRanklist(data.ranklist);
    //   dispatch({
    //     type: 'SET_RANK_LIST',
    //     payload: data.ranklist
    //   });
    // });

    // ====TEMPORARY DUMMY DATA=======//
    setRanklist([{ pic: "pic1", name: "name1", rank: "1", level: "4" }, { pic: "pic2", name: "name2", rank: "2", level: "2" }, { pic: "pic3", name: "name3", rank: "3", level: "1" }])

  }, [dispatch]);

  const RankListRows = ({ ranklist }: { ranklist: rankList[] }) => (
    <>
      {ranklist.map((curr: rankList) => (
        <tr>
          <td>
            <img src={curr.pic} alt="" className="propic" />
          </td>
          <td>{curr.name}</td>
          <td>{curr.rank}</td>
          <td>{curr.level}</td>
        </tr>
      ))}
    </>
  );

  return (
    <div className="ranklist">
      <h2>Ranklist</h2>
      <table className="table">
        <thead>
          <tr>
            <th />
            <th>Name</th>
            <th>Rank</th>
            <th>Level</th>
          </tr>
        </thead>
        <tbody>
          <RankListRows ranklist={ranklist} />
        </tbody>
      </table>
    </div>
  );
};

export default KryptosRanklist;

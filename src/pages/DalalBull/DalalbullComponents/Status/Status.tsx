import React, { useState, useEffect ,FunctionComponent} from 'react';
import { getUserDetail } from '../apicalls/apicalls';
import './Status.scss';

interface Props {
  rank : number,
  cash_bal:number,
  net_worth : number,
  margin : number
}


const Status : FunctionComponent<Props> = props => {
  const [userDetail, setUserDetail] = useState({pic : '' , name: 'Person1'});
  /*useEffect(() => {
    getUserDetail().then((res:any) => setUserDetail(res));
    setUserDetail(data);
  }, []);*/
  return (
    <div className="userdata">
      <div align-self="center">
        <img src={userDetail.pic } alt="" className="propic" align-self="center" />
      </div>
      <h1 className="h1" text-align="center">
        {userDetail.name}
      </h1>
      <br />
      <div className=" user-data">
        <div className=" row">
          <div className=" col-lg-6">
            <h1>{props.rank}</h1>
            <h3>Rank</h3>
          </div>
          <div className=" col-lg-6">
            <h1>{props.net_worth}</h1>
            <h3>Net worth</h3>
          </div>
        </div>
        <div className=" row">
          <div className=" col-lg-6">
            <h1>{props.cash_bal}</h1>
            <h3>Cash available</h3>
          </div>
          <div className=" col-lg-6">
            <h1>{props.margin}</h1>
            <h3>Margin</h3>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Status;

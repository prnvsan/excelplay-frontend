import React from 'react';
import { useSelector } from 'react-redux';
import { Route, RouteProps, Redirect } from 'react-router-dom';
import Spinner from '../Spinner/Spinner';

interface PrivateRouteProps extends RouteProps {
  component: any;
};

const PrivateRoute = (props: PrivateRouteProps) => {
  const { component: Component, ...rest } = props;

  const { isAuthenticated } = useSelector(
    (state: any) => {
      return {
        // isAuthenticated: state.auth.token,
        isAuthenticated: "YES"   //TEMPORARY MODIFIED FOR ADDING DUMMY DATA. Remove this and uncomment previous line when integrating with backend.
      };
    }
  );

  if (isAuthenticated === '') {
    return (<Spinner />);
  } else {
    return (
      <Route
        {...rest}
        render={(routeProps) =>
          isAuthenticated
            ? (<Component {...routeProps} />)
            : (
              <Redirect
                to={{
                  pathname: '/login',
                  state: { from: routeProps.location }
                }}
              />
            )
        }
      />
    );
  }
};

export default PrivateRoute;